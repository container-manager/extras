#!/bin/bash
set -euxo pipefail

cd "$(dirname "$0")"

iso=$1
output_iso="$(dirname $iso)/$(basename $iso .iso)-custom.iso"

prefix="/tmp/container_manager_iso"

iso_mount="$prefix/iso_mnt"
iso_contents="$prefix/iso_contents"

mkdir -p $prefix
mkdir -p $iso_mount
mkdir -p $iso_contents

mount $iso $iso_mount
cp -arT $iso_mount $iso_contents
umount $iso_mount

txt_config="$iso_contents/isolinux/txt.cfg"
isolinux_config="$iso_contents/isolinux/isolinux.cfg"

kernel_config="file=/cdrom/preseed/ubuntu-server.seed auto=true priority=high preseed/file=/cdrom/preseed/custom-server.seed"

txt_block="label custom-install\n\
  menu label ^Custom Install\n\
  kernel /install/vmlinuz\n\
  append append initrd=/install/initrd.gz $kernel_config ---\n"

sed -i "s|default install|default custom-install\n$txt_block|" $txt_config
sed -i 's/timeout 300/timeout 10/' $isolinux_config

cp custom-server.seed $iso_contents/preseed/

genisoimage -o $output_iso -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -J -R -V CUSTOM_UBUNTU $iso_contents
isohybrid $output_iso
