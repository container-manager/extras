#!/bin/bash
set -exo pipefail

cd $(dirname $0)

export PATH=$HOME/go/bin:$PATH

build_env() {
	res=""
	for var in $(env | grep -i 'proxy\|CI'); do
		res="$res --build-arg $var "
	done
	echo $res
}

for dir in $(ls -d $1*/); do
	name=$(basename $dir)
	cd $name

	mkdir -p rootfs
	rm -rf rootfs/*
	mkdir -p lxd
	rm -rf lxd/*

	image=dist-$name
	docker build $(build_env) -t $image .
	docker run -it --name $name $image /bin/true

	cd rootfs
	docker export $name | tar x
	cd ..
	docker rm -f $name

	distrobuilder pack-lxd distrobuilder.yaml rootfs/ lxd/
	cd ..
done
