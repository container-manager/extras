#!/bin/bash
set -euxo pipefail

cd $(dirname $0)

export PATH=$HOME/go/bin:$PATH

for dir in $(ls -d $1*/); do
	name=$(basename $dir)
	cd $name/lxd
	lxc image import lxd.tar.xz rootfs.squashfs --alias $name
	cd ../..
done
