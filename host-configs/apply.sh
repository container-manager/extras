#!/bin/bash

cd "$(dirname "$0")"

for script in $(find scripts -name '*.sh'); do
	./$script
done
