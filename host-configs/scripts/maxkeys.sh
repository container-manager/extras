#!/bin/bash

# This configuration is required because docker uses the kernel keyring (and quickly fills it up)

sysctl kernel.keys.maxkeys=50000
echo kernel.keys.maxkeys=50000 > /etc/sysctl.d/10-maxkeys.conf
