#!/bin/bash

sshConfig="/etc/ssh/sshd_config"
shouldRestart=false

# $1: config key
# $2: config value
set_config() {
	if ! grep -q "^$1 $2$" $sshConfig ; then
		# delete any existing port configuration
		sed -i "/.*$1 .*/d" $sshConfig
		echo "$1 $2" >> $sshConfig 

		shouldRestart=true
	fi
}

handle_restart() {
	if [[ $shouldRestart = true ]]; then
		echo "Restaring sshd"
		systemctl restart sshd
	fi
}

set_config Port 2222
set_config ClientAliveInterval 15m
set_config ClientAliveCountMax 0
set_config PasswordAuthentication no
set_config Banner /etc/ssh/banner

handle_restart
