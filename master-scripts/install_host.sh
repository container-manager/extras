#!/bin/bash
set -euxo pipefail
cd "$(dirname "$0")"

target=$1

ssh='ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'
rsync -e "$ssh" -avr .. root@$target:/root/container-manager-extras
$ssh root@$target /root/container-manager-extras/host-install/install.sh

# ensure ssh control socket is closed (we change the ssh port in install.sh)
ssh -O stop root@$target

# trust key after install
ssh -p 2222 -o StrictHostKeyChecking=accept-new root@$target reboot
