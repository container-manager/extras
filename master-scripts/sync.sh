#!/bin/bash

cd "$(dirname "$0")"
hostname=$(hostname)
sshOpts='-p 2222'

sync_opt() {
	host=$1
	rsync -e "ssh $sshOpts" -a /opt/deploy $host:/opt/
	rsync -e "ssh $sshOpts" -a /opt/cuda $host:/opt/
}

sync_env() {
	host=$1
	rsync -e "ssh $sshOpts" -a /etc/environment $host:/etc/
}

sync_lxc_config() {
	host=$1
	su - gpu -c "lxc ls"
	cp -r /home/gpu/snap/lxd/current/.config/lxc/ /home/gpu/lxc-config
	rsync -e "ssh $sshOpts" -a /home/gpu/lxc-config/ $host:/home/gpu/lxc-config
}

sync_images() {
	host=$1
	for image in $(lxc image ls -c l --format csv); do
		su - gpu -c "lxc image copy $hostname:$image $host: --alias $image"
	done
}

sync_mounts() {
	host=$1

	rsync -e "ssh $sshOpts" -a /etc/auto* $host:/etc/
	ssh $sshOpts $host 'systemctl restart autofs'
}

sync_file() {
	host=$1
	file=$2
	rsync -e "ssh $sshOpts" -a $file $host:$file
}

run_command() {
	host=$1
	command=$2
	ssh -t $sshOpts $host "$command"
}

sync_config() {
	host=$1

	rsync -e "ssh $sshOpts" -a .. $host:/root/container-manager-extras
	ssh $sshOpts $host /root/container-manager-extras/host-configs/apply.sh
	ssh $sshOpts $host 'bash -l -c /root/container-manager-extras/cloud-init/profile.sh'
}

sync_keys() {
	host=$1
	rsync -e "ssh $sshOpts" -a /root/.ssh/ $host:/root/.ssh
}

sync_host() {
	host=$1

	sync_opt $host
	sync_env $host
	sync_lxc_config $host
	sync_images $host
	sync_mounts $host
	sync_config $host
	sync_keys $host
}

target='sync_host'
if [[ -n $1 ]]; then
	target="$1"
fi

if [[ -n $2 ]]; then
	$target root@$2 $3
else
	for host in $(cat hostlist); do
		echo -e "\033[1m***** $host *****\033[0m"
		$target "root@$host" "$3"
		echo ""
	done
fi
